package Demo;

import org.testng.annotations.Test;

import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;
import testlink.api.java.client.TestLinkAPIResults;

import org.testng.annotations.BeforeClass;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;

public class NewTest1 {
	public static WebDriver driver;
	public static String KEY = "a49faf4a67bced205accc282ee42a287";
	public static String URL = "http://localhost:8989/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	public static String PROJECT_NAME = "TestProject";
	public static String PLAN_NAME = "TestPlanDemo";
	public static String EXECUTE_NAME = "TestBuild";

	public void updateTestLinkResult(String testCase, String exception, String result) throws TestLinkAPIException {
		TestLinkAPIClient testlinkAPIClient = new TestLinkAPIClient(KEY, URL);
		testlinkAPIClient.reportTestCaseResult(PROJECT_NAME, PLAN_NAME, testCase, EXECUTE_NAME, exception, result);
	}

	@BeforeClass
	public void beforeClass() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--enable-maximized");
		// options.addArguments("headless");
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("credentials_enable_service", false);
		prefs.put("profile.password_manager_enable", false);
		options.setExperimentalOption("prefs", prefs);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void f() throws TestLinkAPIException {
		String result = "";
		String exception = null;
		try {
			driver.manage().window().maximize();
			// Enter your applicaton URL.
			driver.get("http://google.com");
			result = TestLinkAPIResults.TEST_PASSED;
			updateTestLinkResult("1-2", null, result);
		} catch (Exception e) {
			result = TestLinkAPIResults.TEST_FAILED;
			exception = e.getMessage();
			updateTestLinkResult("1-2", exception, result);
		}
	}

	@AfterClass
	public void afterClass() {
		// Close browser
		driver.close();
		// Kill driver
		driver.quit();
	}

}
